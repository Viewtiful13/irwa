# Summary

* [Introduction](index.md)
* [Assumptions](assumptions.md)
* [Let's Get Started](lets-get-started.md)
* [Set Up ORM & Database](set-up-orm-database.md)
* [Write the First Migration](write-the-first-migration.md)
* [Create a Database Access Layer](create-a-database-access-layer.md)
* [Create a REST API Layer](create-a-rest-api-layer.md)
* [Create a Browser-Based Frontend UI](create-a-browser-based-frontend-ui.md)
