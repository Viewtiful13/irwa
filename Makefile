all: book/index.html

book/index.html: src/*.md
	mdbook build

deploy: book/index.html
	# Assumes ssh agent vars are present in the environment.
	rsync --exclude='*.swp' -avz book/ booksite:/srv/erwabook.com/www-static/intro/
